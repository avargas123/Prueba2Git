BROKER SCHEMA com.itau.integracion.conductafinanciera
PATH com.itau.integracion.comunes;
--DECLARE nsSPConectorRes NAMESPACE 'http://www.itau.com.ar/cntr/sp/response';


CREATE COMPUTE MODULE ConsultarDetalleComportamiento_subflow_Mapeo_Response
	CREATE FUNCTION Main() RETURNS BOOLEAN
	BEGIN
		CALL CopyMessageHeaders();
		
		CREATE FIELD OutputRoot.XMLNSC.nsConDetComRes:ResponseMessage.nsConDetComRes:header;
		DECLARE refOutHeader REFERENCE TO OutputRoot.XMLNSC.nsConDetComRes:ResponseMessage.nsConDetComRes:header;
				
		CALL MapeaCabeceraExito(Environment.Variables.InputHeader, refOutHeader);
		
		CREATE FIELD OutputRoot.XMLNSC.nsConDetComRes:ResponseMessage.nsConDetComRes:body.nsConDetCom:ConsultarDetalleComportamientoResponse;
		DECLARE refOutBody REFERENCE TO OutputRoot.XMLNSC.nsConDetComRes:ResponseMessage.nsConDetComRes:body.nsConDetCom:ConsultarDetalleComportamientoResponse;
		DECLARE refInBody REFERENCE TO InputRoot.SOAP.Body.*:ResponseMessage.*:body.*:response.*:registros.*:registro[1];
		
		CREATE FIELD refOutBody.nsConDetCom:cliente;
		
		DECLARE refOutCli REFERENCE TO refOutBody.nsConDetCom:cliente; 		
		SET refOutCli.nsConDetCom:numeroInternoVeraz 													= refInBody.*:PER_REFERENCIA;
		SET refOutCli.nsConDetCom:documento.nsConDetCom:tipoDocumento									= refInBody.*:TIPO_DOCUMENTO;
		SET refOutCli.nsConDetCom:documento.nsConDetCom:numeroDocumento									= refInBody.*:DOCUMENTO;
		SET refOutCli.nsConDetCom:nombre																= refInBody.*:NOMBRE;
		SET refOutCli.nsConDetCom:sexo																	= refInBody.*:SEXO;
		
		SET refOutCli.nsConDetCom:otrosDocumentos.nsConDetCom:documento[1].nsConDetCom:tipoDocumento			= refInBody.*:TIPO_DOCUMENTO2;
		SET refOutCli.nsConDetCom:otrosDocumentos.nsConDetCom:documento[1].nsConDetCom:numeroDocumento		= refInBody.*:DOCUMENTO2;
		SET refOutCli.nsConDetCom:otrosDocumentos.nsConDetCom:documento[2].nsConDetCom:tipoDocumento			= refInBody.*:TIPO_DOCUMENTO3;
		SET refOutCli.nsConDetCom:otrosDocumentos.nsConDetCom:documento[2].nsConDetCom:numeroDocumento		= refInBody.*:DOCUMENTO3;
		
		SET refOutBody.nsConDetCom:riesgoScore															= refInBody.*:RIESGO_SCORE;
		SET refOutBody.nsConDetCom:atraso6Meses															= refInBody.*:RIESGO_6;
		SET refOutBody.nsConDetCom:cantidadProductos													= refInBody.*:USO_CANT;
		SET refOutBody.nsConDetCom:saldosAcumulados														= refInBody.*:USO_SALDO;
		SET refOutBody.nsConDetCom:limitesCreditosAcumulados											= refInBody.*:USO_LIMITE;
		SET refOutBody.nsConDetCom:pagoMinimoAcumulado													= refInBody.*:USO_EXIGIBLE;
		SET refOutBody.nsConDetCom:mesesDesdeUltimaUtilizacion											= refInBody.*:ACTIVIDAD;
		SET refOutBody.nsConDetCom:cuentasAbiertasUltimos6Meses											= refInBody.*:ACT_OPEN;
		SET refOutBody.nsConDetCom:mesesPrimerUso														= refInBody.*:ANTIGUEDAD;
		SET refOutBody.nsConDetCom:productosAtrasados													= refInBody.*:COB_CANT;
		SET refOutBody.nsConDetCom:saldosVencidosAcumulados												= refInBody.*:COB_VENCIDO;
		SET refOutBody.nsConDetCom:statusMaximoUltimos6Meses											= refInBody.*:OB_STATUS;
		SET refOutBody.nsConDetCom:numeroClienteMasterCard												= refInBody.*:CLIENTE_MC;
		SET refOutBody.nsConDetCom:numeroClienteVisa													= refInBody.*:CLIENTE_VI;
		SET refOutBody.nsConDetCom:numeroClientesOtros													= refInBody.*:CLIENTE_XX;
		SET refOutBody.nsConDetCom:identificadorCliente													= refInBody.*:CLIENTE;
		SET refOutBody.nsConDetCom:clienteConTarjeta													= refInBody.*:CLIENTE_TC;
		SET refOutBody.nsConDetCom:clienteCtaCte														= refInBody.*:CLIENTE_CC;
		SET refOutBody.nsConDetCom:clientePrestamosHipotecarios											= refInBody.*:CLIENTE_GH;
		SET refOutBody.nsConDetCom:clientePrestamosPrendarios											= refInBody.*:CLIENTE_GP;
		SET refOutBody.nsConDetCom:clientePrestamosSinGarantia											= refInBody.*:CLIENTE_SG;
		SET refOutBody.nsConDetCom:clienteOtrosProductos												= refInBody.*:CLIENTE_OT;
		SET refOutBody.nsConDetCom:hit																	= refInBody.*:HIT;
		
		CREATE FIELD refOutBody.nsConDetCom:comportamientoTarjetasCreditos;
		DECLARE refOutComTarCre REFERENCE TO refOutBody.nsConDetCom:comportamientoTarjetasCreditos;
		
		SET refOutComTarCre.nsConDetCom:tendencia														= refInBody.*:FIDELIDAD_TC;
		SET refOutComTarCre.nsConDetCom:cantidadTarjetas												= refInBody.*:USO_TC_CANT;
		SET refOutComTarCre.nsConDetCom:cantidadConMovimientosUltimos6Meses								= refInBody.*:USO_TC_CANT_A;
		SET refOutComTarCre.nsConDetCom:saldosAcumulados 												= refInBody.*:USO_TC_SALDO;
		SET refOutComTarCre.nsConDetCom:limitesCreditos                                                 = refInBody.*:USO_TC_LIMITE;
		SET refOutComTarCre.nsConDetCom:pagosMinimos													= refInBody.*:USO_TC_EXIGIBLE;
		SET refOutComTarCre.nsConDetCom:promedioUltimo3Meses											= refInBody.*:USO_TC_PROMEDIO3;
		SET refOutComTarCre.nsConDetCom:promedioUltimo6Meses											= refInBody.*:USO_TC_PROMEDIO6;
		SET refOutComTarCre.nsConDetCom:limiteMaximo													= refInBody.*:USO_TC_LIMITE_MAX;
		SET refOutComTarCre.nsConDetCom:mesesDesdeUltimaUtilizacion										= refInBody.*:ACTIVIDAD_TC;
		SET refOutComTarCre.nsConDetCom:cantidadAbiertasUltimos6Meses									= refInBody.*:ACT_OPEN_TC;
		SET refOutComTarCre.nsConDetCom:mesesPrimeraUtilizacion											= refInBody.*:ANTIGUEDAD_TC;
		SET refOutComTarCre.nsConDetCom:cantidadConAtraso												= refInBody.*:COB_TC_CANT;
		SET refOutComTarCre.nsConDetCom:saldoVencido													= refInBody.*:COB_TC_VENCIDO;
		SET refOutComTarCre.nsConDetCom:maximoStatusUltimos6Meses										= refInBody.*:COB_TC_STATUS;
		
		CREATE FIELD refOutBody.nsConDetCom:comportamientoPrestamosPrendarios;
		DECLARE refOutComPresPen REFERENCE TO refOutBody.nsConDetCom:comportamientoPrestamosPrendarios;
		
		SET refOutComPresPen.nsConDetCom:cantidadPrestamos												= refInBody.*:USO_GP_CANT;
		SET refOutComPresPen.nsConDetCom:saldosAcumulados												= refInBody.*:USO_GP_SALDO;
		SET refOutComPresPen.nsConDetCom:monto															= refInBody.*:USO_GP_LIMITE;
		SET refOutComPresPen.nsConDetCom:sumaExigible													= refInBody.*:USO_GP_EXIGIBLE;
		SET refOutComPresPen.nsConDetCom:mesesDesdeUltimaUtilizacion									= refInBody.*:ACTIVIDAD_GP;
		SET refOutComPresPen.nsConDetCom:nuevosPrestamosUltimos6Meses									= refInBody.*:ACT_OPEN_GP;
		SET refOutComPresPen.nsConDetCom:mesesPrimeraUtilizacion										= refInBody.*:ANTIGUEDAD_GP;
		SET refOutComPresPen.nsConDetCom:cantidadConAtraso												= refInBody.*:COB_GP_CANT;
		SET refOutComPresPen.nsConDetCom:saldoVencido													= refInBody.*:COB_GP_VENCIDO;
		SET refOutComPresPen.nsConDetCom:maximoStatusUltimos6Meses										= refInBody.*:COB_GP_STATUS;
		SET refOutComPresPen.nsConDetCom:codigoCliente													= refInBody.*:COD_CLIENTE_GP;
		
		CREATE FIELD refOutBody.nsConDetCom:comportamientoPrestamosSinGarantias;
		DECLARE refOutComPresSinGar REFERENCE TO refOutBody.nsConDetCom:comportamientoPrestamosSinGarantias;
		
		SET refOutComPresSinGar.nsConDetCom:cantidadPrestamos											= refInBody.*:USO_SG_CANT;
		SET refOutComPresSinGar.nsConDetCom:saldosAcumulados											= refInBody.*:USO_SG_SALDO;
		SET refOutComPresSinGar.nsConDetCom:monto														= refInBody.*:USO_SG_LIMITE;
		SET refOutComPresSinGar.nsConDetCom:sumaExigible												= refInBody.*:USO_SG_EXIGIBLE;
		SET refOutComPresSinGar.nsConDetCom:mesesDesdeUltimaUtilizacion									= refInBody.*:ACTIVIDAD_SG;
		SET refOutComPresSinGar.nsConDetCom:nuevosPrestamosUltimos6Meses								= refInBody.*:ACT_OPEN_SG;
		SET refOutComPresSinGar.nsConDetCom:mesesPrimeraUtilizacion										= refInBody.*:ANTIGUEDAD_SG;
		SET refOutComPresSinGar.nsConDetCom:cantidadConAtraso											= refInBody.*:COB_SG_CANT;
		SET refOutComPresSinGar.nsConDetCom:saldoVencido												= refInBody.*:COB_SG_VENCIDO;
		SET refOutComPresSinGar.nsConDetCom:maximoStatusUltimos6Meses									= refInBody.*:COB_SG_STATUS;
		SET refOutComPresSinGar.nsConDetCom:codigoCliente												= refInBody.*:COD_CLIENTE_SG;
		
		CREATE FIELD refOutBody.nsConDetCom:comportamientoPrestamosHipotecarios;
		DECLARE refOutComPresHipo REFERENCE TO refOutBody.nsConDetCom:comportamientoPrestamosHipotecarios;
		
		SET refOutComPresHipo.nsConDetCom:cantidadPrestamos												= refInBody.*:USO_GH_CANT;
		SET refOutComPresHipo.nsConDetCom:saldosAcumulados												= refInBody.*:USO_GH_SALDO;
		SET refOutComPresHipo.nsConDetCom:monto															= refInBody.*:USO_GH_LIMITE;
		SET refOutComPresHipo.nsConDetCom:sumaExigible													= refInBody.*:USO_GH_EXIGIBLE;
		SET refOutComPresHipo.nsConDetCom:mesesDesdeUltimaUtilizacion									= refInBody.*:ACTIVIDAD_GH;
		SET refOutComPresHipo.nsConDetCom:nuevosPrestamosUltimos6Meses									= refInBody.*:ACT_OPEN_GH;
		SET refOutComPresHipo.nsConDetCom:mesesPrimeraUtilizacion										= refInBody.*:ANTIGUEDAD_GH;
		SET refOutComPresHipo.nsConDetCom:cantidadConAtraso												= refInBody.*:COB_GH_CANT;
		SET refOutComPresHipo.nsConDetCom:saldoVencido													= refInBody.*:COB_GH_VENCIDO;
		SET refOutComPresHipo.nsConDetCom:maximoStatusUltimos6Meses										= refInBody.*:COB_GH_STATUS;
		SET refOutComPresHipo.nsConDetCom:codigoCliente													= refInBody.*:COD_CLIENTE_GH;
		
		CREATE FIELD refOutBody.nsConDetCom:comportamientoCuentasCorrientes;
		DECLARE refOutComCtasCtes REFERENCE TO refOutBody.nsConDetCom:comportamientoCuentasCorrientes;
		
		SET refOutComCtasCtes.nsConDetCom:tendencia														= refInBody.*:FIDELIDAD_CC;
		SET refOutComCtasCtes.nsConDetCom:cantidadCuentas												= refInBody.*:USO_CC_CANT;
		SET refOutComCtasCtes.nsConDetCom:cantidadConMovimientosUltimos6Meses							= refInBody.*:USO_CC_CANT_A;
		SET refOutComCtasCtes.nsConDetCom:saldosAcumulados												= refInBody.*:USO_CC_SALDO;
		SET refOutComCtasCtes.nsConDetCom:limitesDescubierto											= refInBody.*:USO_CC_LIMITE;
		SET refOutComCtasCtes.nsConDetCom:sumaExigible													= refInBody.*:USO_CC_EXIGIBLE;
		SET refOutComCtasCtes.nsConDetCom:promedioUltimo3Meses											= refInBody.*:USO_CC_PROMEDIO3;
		SET refOutComCtasCtes.nsConDetCom:promedioUltimo6Meses											= refInBody.*:USO_CC_PROMEDIO6;
		SET refOutComCtasCtes.nsConDetCom:limiteMaximo													= refInBody.*:USO_CC_LIMITE_MAX;
		SET refOutComCtasCtes.nsConDetCom:mesesDesdeUltimaUtilizacion									= refInBody.*:ACTIVIDAD_CC;
		SET refOutComCtasCtes.nsConDetCom:cantidadAbiertasUltimos6Meses									= refInBody.*:ACT_OPEN_CC;
		SET refOutComCtasCtes.nsConDetCom:mesesPrimeraUtilizacion										= refInBody.*:ANTIGUEDAD_CC;
		SET refOutComCtasCtes.nsConDetCom:cantidadConAtraso												= refInBody.*:COB_CC_CANT;
		SET refOutComCtasCtes.nsConDetCom:saldoVencido													= refInBody.*:COB_CC_VENCIDO;
		SET refOutComCtasCtes.nsConDetCom:maximoStatusUltimos6Meses										= refInBody.*:COB_CC_STATUS;
		SET refOutComCtasCtes.nsConDetCom:numeroCliente													= refInBody.*:COD_CLIENTE_CC;
		
		
		RETURN TRUE;
	END;

	CREATE PROCEDURE CopyMessageHeaders() BEGIN
		DECLARE I INTEGER 1;
		DECLARE J INTEGER;
		SET J = CARDINALITY(InputRoot.*[]);
		WHILE I < J DO
			SET OutputRoot.*[I] = InputRoot.*[I];
			SET I = I + 1;
		END WHILE;
	END;

	CREATE PROCEDURE CopyEntireMessage() BEGIN
		SET OutputRoot = InputRoot;
	END;
END MODULE;
