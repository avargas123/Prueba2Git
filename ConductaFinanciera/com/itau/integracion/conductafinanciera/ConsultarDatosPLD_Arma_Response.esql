BROKER SCHEMA com.itau.integracion.conductafinanciera
PATH com.itau.integracion.comunes;

CREATE COMPUTE MODULE ConsultarDatosPLD_Arma_Response
	CREATE FUNCTION Main() RETURNS BOOLEAN
	BEGIN
		
		DECLARE refEnv REFERENCE TO Environment.Variables.CDPLD;
		DECLARE refProp REFERENCE TO InputRoot.Properties;

		CALL CopyMessageHeaders();

		SET OutputRoot.MQMD = NULL;
		SET OutputRoot.Properties.MessageSet = '';
		SET OutputRoot.Properties.MessageType = '';
		SET OutputRoot.Properties.MessageFormat = '';

		IF EXISTS(InputRoot.XMLNSC.connectorMessage.Respuesta[]) THEN
			-- Parsear dato del tag <Respuesta> en arbol
			DECLARE strMje CHARACTER InputRoot.XMLNSC.connectorMessage.Respuesta;
			DECLARE blMje BLOB CAST(strMje AS BLOB CCSID 819);
			CREATE LASTCHILD OF refEnv DOMAIN('MRM') PARSE(blMje, 273, 819, 'DOSCBKMSP', 'DOSCBKRES', 'Binary1');

		END IF;
		DECLARE refInHeader REFERENCE TO refEnv.MRM.HEADER;
		DECLARE refAsBody REFERENCE TO refEnv.MRM.DATA; --refEnv
		
		
		-- Determinar exito o error
		IF EXISTS(InputRoot.XMLNSC.connectorMessage.Error[]) THEN

			CREATE FIELD OutputRoot.XMLNSC.nsConsDatPLDRes:ResponseMessage.nsConsDatPLDRes:header;
			DECLARE refOutHeader REFERENCE TO OutputRoot.XMLNSC.nsConsDatPLDRes:ResponseMessage.nsConsDatPLDRes:header;

			CALL MapeaCabeceraErrorLegado(refEnv.InputHeader, refOutHeader); -- TODO: Definir que tipo de error mandamos

			CREATE FIELD OutputRoot.XMLNSC.nsConsDatPLDRes:ResponseMessage.nsConsDatPLDRes:body.nsConsDatPLD:Error.detalle;
			DECLARE refEsbBody REFERENCE TO OutputRoot.XMLNSC.nsConsDatPLDRes:ResponseMessage.nsConsDatPLDRes:body.nsConsDatPLD:Error.detalle;

			SET refEsbBody.codigo = '01';
			SET refEsbBody.subCodigo = InputRoot.XMLNSC.connectorMessage.Error.Subcodigo;
			SET refEsbBody.descripcion = 'Detalle retornado por el conector: ' || InputRoot.XMLNSC.connectorMessage.Error.Mensaje;
			SET refEsbBody.origen = 'Conector AS400';
			

			RETURN TRUE;

		ELSEIF refInHeader.CODRET <> '00' THEN -- Si AS400 retorno error, mapear rta de la operacion con detalles

			CREATE FIELD OutputRoot.XMLNSC.nsConsDatPLDRes:ResponseMessage.nsConsDatPLDRes:header;
			DECLARE refOutHeader REFERENCE TO OutputRoot.XMLNSC.nsConsDatPLDRes:ResponseMessage.nsConsDatPLDRes:header;

			CALL MapeaCabeceraErrorLegado(refEnv.InputHeader, refOutHeader);

			CREATE FIELD OutputRoot.XMLNSC.nsConsDatPLDRes:ResponseMessage.nsConsDatPLDRes:body.nsConsDatPLD:Error.detalle;
			DECLARE refEsbBody REFERENCE TO OutputRoot.XMLNSC.nsConsDatPLDRes:ResponseMessage.nsConsDatPLDRes:body.nsConsDatPLD:Error.detalle;

			SET refEsbBody.codigo = FIELDVALUE(refInHeader.CODRET);
			SET refEsbBody.subCodigo = FIELDVALUE(refInHeader.MSGID);
			SET refEsbBody.descripcion = 'Detalle retornado por el proveedor: ' || FIELDVALUE(refInHeader.MSGID);
			SET refEsbBody.origen = 'DOSCBK';
			SET refEsbBody.tipo = ObtenerIdentificador_As400();

			RETURN TRUE;

		ELSE -- Si la rta del backend es exitosa

			CREATE FIELD OutputRoot.XMLNSC.nsConsDatPLDRes:ResponseMessage.nsConsDatPLDRes:header;
			DECLARE refOutHeader REFERENCE TO OutputRoot.XMLNSC.nsConsDatPLDRes:ResponseMessage.nsConsDatPLDRes:header;

			CALL MapeaCabeceraExito(refEnv.InputHeader, refOutHeader);

			CREATE FIELD OutputRoot.XMLNSC.nsConsDatPLDRes:ResponseMessage.nsConsDatPLDRes:body.nsConsDatPLD:ConsultarDatosPLDResponse;
			DECLARE refOutBody REFERENCE TO OutputRoot.XMLNSC.nsConsDatPLDRes:ResponseMessage.nsConsDatPLDRes:body.nsConsDatPLD:ConsultarDatosPLDResponse;
			
			SET refOutBody.nsConsDatPLD:codigoActividadBCRA 			= CAST(FIELDVALUE(refAsBody.CODACTIV) AS INTEGER);
			SET refOutBody.nsConsDatPLD:descripcionActividadBCRA 		= TRIM( FIELDVALUE(refAsBody.DESCACTIV));
			SET refOutBody.nsConsDatPLD:declaracionJurada 				= TRIM( FIELDVALUE(refAsBody.INDDJACT));
			SET refOutBody.nsConsDatPLD:fechaCierreBalance 				= TRIM( FechaAS400ToEsb(FIELDVALUE(refAsBody.DTCIERRE)));
			SET refOutBody.nsConsDatPLD:montoFacturacionVta 			= CaracterToDecimal(FIELDVALUE(refAsBody.MTOFACVTA), 0);
			SET refOutBody.nsConsDatPLD:perfilCliente 					= CaracterToDecimal(FIELDVALUE(refAsBody.PERFIL), 0);
			SET refOutBody.nsConsDatPLD:riesgo			 				= CaracterToDecimal(FIELDVALUE(refAsBody.RIESGO), 2);
			SET refOutBody.nsConsDatPLD:operacionesCambio 				= TRIM( FIELDVALUE(refAsBody.OPERCBIO));
			SET refOutBody.nsConsDatPLD:participacionSocExt 			= TRIM( FIELDVALUE(refAsBody.PARTSOCEXT));
			SET refOutBody.nsConsDatPLD:operacionesFactoring			= TRIM( FIELDVALUE(refAsBody.OPERFACT));
			SET refOutBody.nsConsDatPLD:fechaVencimiento				= FechaAS400ToEsb(FIELDVALUE(refAsBody.DTVENC));
			SET refOutBody.nsConsDatPLD:ramo	      					= refAsBody.RAMO ;
			SET refOutBody.nsConsDatPLD:indicadorFACTA 					= refAsBody.INDFATCA;

			--CRS
			DECLARE refOutCRS	REFERENCE TO  OutputRoot.XMLNSC.nsConsDatPLDRes:ResponseMessage.nsConsDatPLDRes:body.nsConsDatPLD:ConsultarDatosPLDResponse.nsConsDatPLD:CRS;
			
			SET refOutCRS.nsConsDatPLD:indicadorCRS 	= refAsBody.INDCRS;
			SET refOutCRS.nsConsDatPLD:presentoDDJJCRS 	= refAsBody.INDPDJJCRS;
			
			--Residencias Fiscales
			DECLARE refOutResFis REFERENCE TO OutputRoot.XMLNSC.nsConsDatPLDRes:ResponseMessage.nsConsDatPLDRes:body.nsConsDatPLD:ConsultarDatosPLDResponse.nsConsDatPLD:residenciasFiscales.nsConsDatPLD:residenciaFiscal[1];
			DECLARE refInResFis REFERENCE TO refAsBody.DATOSREFI[1];
			
			WHILE LASTMOVE(refInResFis) DO
				
				IF FIELDVALUE(refInResFis.PAISRESFISCAL) <> ' ' THEN
					SET refOutResFis.nsConsDatPLD:paisResFiscal 	= refInResFis.PAISRESFISCAL;
					SET refOutResFis.nsConsDatPLD:nombreCalle		= refInResFis.CALLE;
					SET refOutResFis.nsConsDatPLD:numeroCalle		= refInResFis.NRODOM;
					SET refOutResFis.nsConsDatPLD:ciudad			= refInResFis.CIUDAD;
					SET refOutResFis.nsConsDatPLD:pais				= refInResFis.PAIS;
					SET refOutResFis.nsConsDatPLD:indicadorNIFTIN	= refInResFis.INDNIFTIN;
					SET refOutResFis.nsConsDatPLD:Marca				= refInResFis.MARCANT;
					SET refOutResFis.nsConsDatPLD:tipoNIFTIN		= refInResFis.NIFTIN;
				END IF;

				MOVE refInResFis NEXTSIBLING;
				MOVE refOutResFis NEXTSIBLING;
			END WHILE;
			
		
			SET refOutBody.nsConsDatPLD:TipoOperacion				= refAsBody.OPTIOP;
			
			--Sujeto Obligado
			DECLARE refOutSujOb	REFERENCE TO  OutputRoot.XMLNSC.nsConsDatPLDRes:ResponseMessage.nsConsDatPLDRes:body.nsConsDatPLD:ConsultarDatosPLDResponse.nsConsDatPLD:SujetoObligado	;
			
			SET refOutSujOb.nsConsDatPLD:indicadorSO				= refAsBody.INDDJJSO;
			SET refOutSujOb.nsConsDatPLD:indicadorDDJJ				= refAsBody.INDPDJJSO;
			SET refOutSujOb.nsConsDatPLD:fechaPresentacionDDJJ		= FechaAS400ToEsb(FIELDVALUE(refAsBody.DTDDJJSO));
			SET refOutSujOb.nsConsDatPLD:indicadorInscripcionUIF	= refAsBody.ININSUIF;
			SET refOutSujOb.nsConsDatPLD:fechaVencConstanciaUIF		= FechaAS400ToEsb(FIELDVALUE(refAsBody.DTVENUIF));
			
			RETURN TRUE;
		END IF;


		RETURN FALSE;
	END;

	CREATE PROCEDURE CopyMessageHeaders() BEGIN
		DECLARE I INTEGER 1;
		DECLARE J INTEGER;
		SET J = CARDINALITY(InputRoot.*[]);
		WHILE I < J DO
			SET OutputRoot.*[I] = InputRoot.*[I];
			SET I = I + 1;
		END WHILE;
	END;

	CREATE PROCEDURE CopyEntireMessage() BEGIN
		SET OutputRoot = InputRoot;
	END;
END MODULE;
